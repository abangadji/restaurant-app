const express = require('express');
const path = require('path');
const fs = require('fs');
const Category = require('../models/Category');

const router = express.Router();


router.get('/', (req, res) => {
	Category.find({})
	.then(data => {
		res.send(data)
	});	
})

router.post('/', (req, res) => {
    let newCategory = new Category (req.body);
    newCategory.save()
    .then(data => {
        res.send(data);
    });
})


router.delete('/:id', (req, res) => {
	Category.findOneAndDelete({_id: req.params.id}, { useFindAndModify: false }) 
	.then(data => {
		res.send(data);
	});
})

router.patch('/:id', (req, res) => {
    Category.updateOne({_id: req.params.id}, req.body, { new: true, useFindAndModify: false })
    .then(data => {
        res.send(data);
    });
})

module.exports = router;