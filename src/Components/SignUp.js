import React from 'react';

class SignUp extends React.Component{
    render(){
        return(
            <div className = "signUpComponent">
                <h2>Hey there, foodie!</h2>

                <div className = "infoPage">
                    <strong>Join our commnity and level up your foodie experience!</strong>
                    <ul>
                        <li>Get Food Delivered</li>
                        <li>Check Order History</li>
                        <li>Check Your Membership Points</li>
                        <li>Claim Exciting Rewards</li>
                    </ul>

                    <strong>Already a member?</strong>
                    <button type = "button" className = "BtnContainer">Sign in</button>
                </div>

                <form>
                    <div>
                        <input type = "text" placeholder = "Full name"/>
                    </div>
                    <div>
                        <input type = "email" placeholder = "Email"/>
                    </div>
                    <div>
                        <input type = "text" placeholder = "Username"/>
                    </div>
                    <div>
                        <input type = "password" placeholder = "Password"/> 
                    </div>
                    <button type="button" className = "BtnContainer">Create Account</button>
                    <div>
                        <a href ="#">Forgot your password?</a>
                    </div>

                </form>
             
            </div>
        )
    }
}

export default SignUp;