import React from 'react';

class Login extends React.Component{
    render(){
        return(
            <div className = "loginComponent clearfix">
                
                <h2>Hey there, foodie!</h2>

                <div className = "infoPage">
                    <strong>Level up your foodie experience!</strong>
                    <ul>
                        <li>Get Food Delivered</li>
                        <li>Check Order History</li>
                        <li>Check Your Membership Points</li>
                        <li>Claim Exciting Rewards</li>
                    </ul>

                    <strong>Not a member yet?</strong>
                    <button type = "button" className = "BtnContainer">Create Account</button>
                </div>

                <form>
                    <div>
                        <input type = "text" placeholder = "Username"/>
                    </div>
                    <div>
                        <input type = "password" placeholder = "Password"/> 
                    </div>
                    <button type="button" className = "BtnContainer">Login</button>
                    <div>
                        <a href ="#">Forgot your password?</a>
                    </div>

                </form>
             


            </div>
        )
    }
}

export default Login;