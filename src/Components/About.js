import React from 'react';
import {BrowserRouter, Route, Link} from 'react-router-dom';

class About extends React.Component{
    render(){
        return(
            <div className = "contentBodyContainer">
                <h2>About Us</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ornare arcu dui vivamus arcu. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Dolor sed viverra ipsum nunc. Phasellus egestas tellus rutrum tellus pellentesque. Felis donec et odio pellentesque diam. Nam libero justo laoreet sit amet. Orci ac auctor augue mauris augue neque gravida. Neque viverra justo nec ultrices dui sapien. Cras tincidunt lobortis feugiat vivamus at augue. Sit amet consectetur adipiscing elit pellentesque. Et netus et malesuada fames ac. Urna id volutpat lacus laoreet non curabitur gravida. Nec dui nunc mattis enim ut tellus.</p>
                 <br/>   
                <p>Commodo nulla facilisi nullam vehicula ipsum a arcu. Dui nunc mattis enim ut tellus elementum. Eget magna fermentum iaculis eu non diam. Etiam tempor orci eu lobortis elementum nibh. Adipiscing elit ut aliquam purus. Suspendisse sed nisi lacus sed viverra tellus in hac habitasse. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Cursus vitae congue mauris rhoncus aenean vel. Magna fringilla urna porttitor rhoncus dolor purus non enim. Cursus eget nunc scelerisque viverra mauris in aliquam. Eget felis eget nunc lobortis mattis.</p>
                 <br/>
                <p>A diam sollicitudin tempor id eu nisl nunc mi. Bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Lacinia quis vel eros donec ac. Fringilla ut morbi tincidunt augue interdum velit. Accumsan lacus vel facilisis volutpat est velit. Pretium nibh ipsum consequat nisl vel pretium. Quisque egestas diam in arcu cursus euismod quis. Mauris augue neque gravida in fermentum. Senectus et netus et malesuada fames ac turpis egestas integer. Facilisis sed odio morbi quis commodo odio aenean sed. Aliquam id diam maecenas ultricies mi eget mauris pharetra et. Habitasse platea dictumst quisque sagittis purus sit amet volutpat consequat. Eros donec ac odio tempor orci. Urna neque viverra justo nec. Vitae aliquet nec ullamcorper sit amet risus nullam eget. Porttitor leo a diam sollicitudin tempor id.</p>
            
            </div>

        )
    }
}

export default About;