import React from 'react';
import Navitem from './Navitem';

class Navbar extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            'NavItemActive':''
        }
    }
    activeitem=(x)=>
    {
        if(this.state.NavItemActive.length>0){
            document.getElementById(this.state.NavItemActive).classList.remove('active');
        }
        this.setState({'NavItemActive':x},()=>{
            document.getElementById(this.state.NavItemActive).classList.add('active');
        });
    };

    render(){
        return(
            <div className = "navBar clearfix">
                    <div className = "headerTop">
                        <h1>Fresh Foods</h1>
                        <h4>for foodies by foodies</h4>
                    </div>

                    <div className = "headerSecond clearfix">
                        <div className = "addContainer">123 Main St., Poblacion, Makati City 1211</div>
                        <div className = "contactContainer">
                            <p>1-800-1234-567</p>
                            <p>Mon - Sun | 10:00AM- 10:00PM</p>
                        </div>
                    </div>
                

                <div className = "navContainer clearfix">
                    <Navitem item="HOME" tolink="/"  activec={this.activeitem}></Navitem>
                    <Navitem item="ABOUT" tolink="/about"  activec={this.activeitem}></Navitem>
                    <Navitem item="MENU" tolink="/menu"  activec={this.activeitem}></Navitem>
                    <Navitem item="DELIVERY" tolink="/delivery"  activec={this.activeitem} ></Navitem>
                    <Navitem item="REVIEWS" tolink="/reviews"  activec={this.activeitem} ></Navitem> 
                    <Navitem item="SIGN IN" tolink="/login"  activec={this.activeitem}></Navitem>
                    <Navitem item="JOIN US" tolink="/join"  activec={this.activeitem} ></Navitem>
                    <Navitem item="CONNECT" tolink="/connect"  activec={this.activeitem}></Navitem>
                </div>

                <div className = "socialMediaContainer clearfix">
                    <img src = "https://image.flaticon.com/icons/svg/1077/1077041.svg" className = "socMed"/>
                    <img src = "https://image.flaticon.com/icons/svg/1077/1077042.svg" className = "socMed"/>
                    <img src = "https://image.flaticon.com/icons/svg/1051/1051382.svg" className = "socMed"/>
                </div>
                
            </div>
        )
    }
}

export default Navbar;