import React from 'react';
import {connect} from 'react-redux';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';

import Navbar from './Components/Navbar';
import Home from './Components/Home';
import About from './Components/About';
import Menu from './Components/Menu';
import Delivery from './Components/Delivery';
import Contact from './Components/Contact';
import Login from './Components/Login';
import SignUp from './Components/SignUp';

const axios = require('axios')

const dispatchCategoryHandler = (category) => {
  return{
    type : "CATEGORY_DATA",
    payload : category
  }
}

const dispatchItemHandler = (items) => {
  return{
    type : "ITEM_DATA",
    payload : items
  }
}

class  App extends React.Component{

  componentDidMount = async() =>{
    const itemRes = await axios.get('http://localhost:8080/item')
    const items = itemRes.data;

    const categoryRes = await axios.get('http://localhost:8080/category')
    const category = categoryRes.data

    this.props.addToState({ 
      items : items,
      category : category
    
    })
    console.log(items);
  }

  render(){
    return(
      <Router>
        <div className = "App clearfix">

        <Navbar/>

          <Route exact path = "/">
            <Home/>
          </Route>

          <Route path = "/about">
            <About/>
          </Route>

          <Route path = "/menu">
            <Menu/>
          </Route>

          {/* <Route path = "/delivery">
            <Delivery state = {this.state}/>
          </Route>

          <Route path = "/reviews">
            <Reviews/>
          </Route>

          <Route path = "/login">
            <Login/>
          </Route>

          <Route path = "/join">
            <SignUp/>
          </Route>

          <Route path = "/connect">
            <Connect/>
          </Route>   */}

          <footer>
            <strong>Fresh Foods © 2020 All Rights Reserved</strong>
          </footer>
        </div>
      </Router>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addToStateCategory : (category) => {dispatch(dispatchCategoryHandler(category))},
    addToStateItem : (items) => {dispatch(dispatchItemHandler(items))}

  }
}

export default connect(null, mapDispatchToProps)(App);
